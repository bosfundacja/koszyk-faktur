from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from .models import InvoiceModel
from .forms import InvoiceForm

# Create your views here.

def invoices_list(request):
    invoices = InvoiceModel.objects.all()
    return render(request, 'invoices.html', {'invoices': invoices})


def invoices_new(request):
    if request.method == 'POST':
        form = InvoiceForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('invoices_list')
    else:
        form = InvoiceForm()
    return render(request, 'invoices_new.html', {'form': form}) 


def invoices_set_paid(request, pk):
    invoice = get_object_or_404(InvoiceModel, pk=pk)
    invoice.paid = True
    invoice.save()
    return redirect('invoices_list')


def invoices_delete(request, pk):
    invoice = get_object_or_404(InvoiceModel, pk=pk)
    invoice.delete()
    return redirect('invoices_list')
