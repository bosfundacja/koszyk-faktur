# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class InvoiceModel(models.Model):
    title = models.CharField(max_length=200)
    value = models.CharField(max_length=200)
    paid = models.BooleanField(default=False)
    invoice_file = models.FileField(upload_to='static')
    
