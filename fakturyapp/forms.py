from django import forms
from .models import InvoiceModel

class InvoiceForm(forms.ModelForm):


    class Meta:
        model = InvoiceModel
            
        fields = ('title', 'value', 'paid', 'invoice_file',)
