from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    url('lista$', views.invoices_list, name='invoices_list'),
    url('nowa$', views.invoices_new, name='invoices_new'),
    url('oplac/(?P<pk>[0-9]+)$', views.invoices_set_paid, name='invoices_set_paid'),
    url('usun/(?P<pk>[0-9]+)$', views.invoices_delete, name='invoices_delete'),
    url('login$', auth_views.login, {'template_name': 'login.html'}, name='login'),
    url('logout$', auth_views.logout, {'next_page': 'invoices_list'}, name='logout'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT + '/static')
